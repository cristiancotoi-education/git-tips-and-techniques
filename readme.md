# Git tip and techniques

This repository contains a few tips (not too many), that I found to be useful throughout my dev experience.
It is aimed to be educational for people just encountering git, and not exhaustive or for advanced users.

Most of the information is grouped into the `docs` folder.

For feedback raise issues on this project or send me a message in private.
