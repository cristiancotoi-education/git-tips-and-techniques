echo -e "\e[33mLoading git helper functions\e[0m"

# Run this from a feature branch
# it switches to master, pulls all the latest changes and re-bases the feature onto master
pull-and-rebase () {
  devBranch=$(git branch | grep \* | cut -d ' ' -f2-)
  onBranch="master"

  echo "Rebasing: $devBranch onto $onBranch"

	if [ "$devBranch" == "$onBranch" ]
	then
		echo -e "\e[31mCannot rebase branch onto itself\e[0m"
		return 1
	fi
    echo -e "\e[214mPulling and rebasing \"$devBranch\"\e[0m"
	echo -e "\e[33mCheckout $onBranch\e[0m"
	git checkout "$onBranch"
	echo -e "\e[33mPull\e[0m"
	git pull --all
	echo -e "\e[33mCheckout $devBranch\e[0m"
	git checkout "$devBranch" 
	echo -e "\e[33mRebase $onBranch\e[0m"
	git rebase "$onBranch"
	echo -e "\e[32mDone\e[0m"
}

# Pulls the latest origin with all branches
# And then cleans up all branches that were merged into master
# This is a sanitization function
cleanup-git-branches () {
	echo -e "\e[33mCheckout master\e[0m"
	git checkout master
	echo -e "\e[33mPull\e[0m"
	git pull --all
	
	echo -e "\e[42mCleanup Git branches\e[0m"
	echo -e "\e[33mCleanup branches removed from origin\e[0m"
	git remote prune origin
	
	echo -e "\e[33mCleanup branches merged into master\e[0m"
	git branch --merged master | grep -v -e "master" -e "development" | xargs -n 1 git branch -d
	
	echo -e "\n\e[46mLocal branches \e[0m"
	git branch --list --all
}

echo -e "\e[33mDone loading helper functions\e[0m"
