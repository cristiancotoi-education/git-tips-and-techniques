---
layout: post
title:  "Git pull --rebase"
date:   2021-07-27 18:03:00 0300
categories: git rebase introduction
---

### {{page.title}}

+[cute article](https://medium.com/anitab-org-open-source/how-i-managed-to-not-mess-up-my-git-history-thanks-to-git-pull-rebase-fed452c661f0)

![git rebase ftw](https://miro.medium.com/max/638/1*bRphImOlKsCoeBmA9rnSmA.jpeg)

Scenario: 2 developers work on the same branch.
They each create 1 commit.

Developer A pushes the code.
Developer's B push will be rejected.
At this point git suggests a `git merge` command. DO NOT DO THIS!
It will work, but the tree will start to look weird.

Instead, run `git pull --rebase`. This will pull the changes from dev A locally,
then replay the changes from dev on top of that.
