---
layout: post
title:  "Getting started with git"
date:   2021-07-27 18:00:00 0300
categories: git introduction
published: true
---

## {{page.title}}

Git is based on a pretty basic idea: take the files, put them in an archive, call it a commit, store it into a folder.

However, when you start working with this in real live,
there are a lot of things that you can do with these commits and around them.

This material focuses mainly on the broad way of working and using git,
and much less on technical aspects. For those just search online.

### Assumptions

This document is based on a few assumptions.

1. The reader is familiar with google and stack-overflow-driven-development.
   Most technical issues can be solved this way.
   However, most mindset and experience-based issues are quite harder to solve.

![Img](https://pbs.twimg.com/media/CcuYraMXIAE5Yn6.jpg:small)

2. The user can make basic commits and pull-push a repository.

### First git video

This is not quite the most popular video on git, but the way this is explained is pure genius.

Watch this before anything else to understand what are: commits, branches, tags
and how they all interplay with one another. 

[![Git for ages 4 and up](https://img.youtube.com/vi/1ffBJ4sVUb4/0.jpg)](https://www.youtube.com/watch?v=1ffBJ4sVUb4)

### Seeing things

A big chunk of git's mysticism comes from the lack of ability to **see** what is happening.
For this, quite a number of tools have appeared, either independent or built in IDEs,
but they all tend to fall short when it comes to rendering complex situations.

The most reliable option to see the git tree is in a console.
