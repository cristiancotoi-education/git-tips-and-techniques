---
layout: post
title:  "Git rebasing"
date:   2021-07-27 18:02:00 0300
categories: git rebase introduction
---

## {{page.title}}

Rebase is very useful, misunderstood and potentially dangerous operation.

### What rebase means

Because a picture is worth 1000 words:

![rebase visual representation](https://cms-assets.tutsplus.com/uploads/users/585/posts/23191/image/rebase.png)

More detailed documentation can be found in [this attlasian article](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)

### Notes

Rebase **recreates** the commits that change, so you will have to
`git push --force` to get your changes across.
Recently a new flag was introduced: `git push --force-with-lease` which will
throw an error if the branch contains changes made by somebody else.

### Why rebase? 

1. In order to pick up changes from the master branch
2. To pick up changes from a colleague's feature branch, before those get into master
3. In order to keep a clean git history

![git tree shape comparison]({{site.baseurl}}/assets/img/git-tree-shape-comparison.png)

4. To maintain a decent git log history: merge commits, update messages, reorder commits

[Stackoverflow if rich in answers](https://stackoverflow.com/questions/804115/when-do-you-use-git-rebase-instead-of-git-merge)
to illuminate the differences and uses of each command.
Their usage will depend on your personal preference and the team coding practices
of each team.

So far, a successful and simple approach is:
rebase onto a branch to pick up changes and resolve conflicts,
then merge when everything is up to date.

Keep in mind that junior developers need some support when doing their first rebases.

### When NOT to rebase?

When working on public branches (master/mainline) rebasing
will lead to everyone else getting an error when pushing.
This should be done only on feature branches, or after extensive discussion
with everyone who will be impacted.

### Practice

1. Always keep an eye on the git tree (e.g. `gl` or `git lg`)
2. Keep in mind logs shows the latest change on top, while rebase shows it at the bottom
3. Rebase ONLY on feature branches.
   You can't quite rebase past merge commits without creating anomalies here and there.
