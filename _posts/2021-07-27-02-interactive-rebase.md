---
layout: post
title:  "Git interactive rebase"
date:   2021-07-27 19:04:00 0300
categories: git interactive rebase
---

### {{page.title}}

Interactive rebase allows you to rename, reorder, merge or delete commits.
Usually this is done to make history right again :).

### Case study - slipped wrong files into an old commit

#### Step 1 - add wrong files

```
* d0103042e0 - (HEAD -> master, origin/master, origin/HEAD) Super feature X (1 minute ago) <John>
* 056a28c440 - Some bulk refactoring (1 days ago) <Mike>
* 329fbe465a - Add skeleton (31 hours ago) <Billy>
* 489a28ea04 - Initial commit with everything (2 days ago) <Denis>
...
```

Let's assume that **Denis** managed to create a commit that includes the whole `/target` folder.
That is a no-go, because every commit will exponentially grow the git repository.

Let's also assume that the rest of the commits don't include changes on `/target`.

**Step 1**: delete the `/target` folder and create a commit with this change

```
* f876fg7311 - (HEAD -> master, origin/master, origin/HEAD) Remove /target (1 minute ago) <John>
* d0103042e0 - Super feature X (1 minute ago) <John>
* 056a28c440 - Some bulk refactoring (1 days ago) <Mike>
* 329fbe465a - Add skeleton (31 hours ago) <Billy>
* 489a28ea04 - Initial commit with everything (2 days ago) <Denis>
... 
```

**Step 2**: use `git rebase -i HEAD~5` to initiate a change on the last 5 commits
(`HEAD~5` = last 5 commits starting from HEAD).
You'll be presented with something like this:

```
pick 489a28ea04 Initial commit with everything
pick 329fbe465a Add skeleton
pick 056a28c440 Some bulk refactoring
pick d0103042e0 Super feature X
pick f876fg7311 Remove /target
```

The first verb on the line describes the action you will take.
Pick means you will take the commit as is with no changes.

In our case we need first to reorder them,
so that the **Remove /target** is right above **Initial commit**.

Once we do that, we change the verb of the **Remove /target** commit to **fixup**.
This will make that commit "blend" into the above one.

```
pick 489a28ea04 Initial commit with everything
fixup f876fg7311 Remove /target
pick 329fbe465a Add skeleton
pick 056a28c440 Some bulk refactoring
pick d0103042e0 Super feature X
```

Once you close the file, if there are no conflicts,
git will get to work and change the **Intial commit..** commit.

You'll notice that once you do this, every commit will change its hash code,
even if the contents looks the same.
That is because the hash is calculated based on previous commits and the current one.

So the new git log will look almost the same, except the hashes.

```
* c9874b9bb9 - (HEAD -> master, origin/master, origin/HEAD) Super feature X (1 minute ago) <John>
* ffa4aac332 - Some bulk refactoring (1 days ago) <Mike>
* 9009ffg009 - Add skeleton (31 hours ago) <Billy>
* 397oba9768 - Initial commit with everything (2 days ago) <Denis>
...
```

In order to get this through to the remote repository,
use `git push --force` or `git push --force-with-lease`,
so you avoid loosing others work.