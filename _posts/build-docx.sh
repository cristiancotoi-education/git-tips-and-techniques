#!/usr/bin/env bash

# docx destination
# commit the result of the command to git, so the business users can pull the latest version
mkdir -p docx

pandoc \
    --from markdown \
    --to docx \
    --output docx/automated-testing.docx \
    \
    2021-07-17-getting-started.md
