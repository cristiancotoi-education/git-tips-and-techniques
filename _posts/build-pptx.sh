#!/usr/bin/env bash

# docx destination
# commit the result of the command to git, so the business users can pull the latest version
mkdir -p docx

pandoc \
    --from markdown \
    --to pptx \
    --output docx/automated-testing.pptx \
    \
    2021-07-17-getting-started.md
