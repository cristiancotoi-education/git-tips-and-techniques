---
layout: post
title:  "Git helper functions"
date:   2021-07-27 18:01:00 0300
categories: git bash helper functions
---

## {{page.title}}

The following is a bunch of minor scripts or aliases to help with git operations.

### Git tree vizualization

#### Git bash alias

Copy these the alias lines into your own `~/.bashrc`.

{% highlight shell %}
# Git
alias glh="git log --graph --all --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias gl="glh -15"

alias git-diff-list="git diff --name-only"
{% endhighlight %}

You should be able to run `glh` or `gl`.
The difference is that `gl` only shows last 15 commits.

![gl output]({{site.baseurl}}/assets/img/gl-output.png)

#### Git alias (in .gitconfig)

A similar result as above can be accomplished by placing aliases in `~/.gitconfig`.
You can see examples in the included [.gitconfig]({{site.baseurl}}/assets/code/gitconfig)

The results are similar, with minor variations.
Now try running any of the following commands `git lg`, `git lg -15`, `git hist`.
Pick your flavor of color and complexity and go with it.

#### Some other helper functions

The following functions are just some flavor for my way of working.
You might need them, or you might not. If you pick something up from this,
it's that you can build your own once you settle on a style.

You can look at them in [git-functions.sh][git functions link].
Keep in mind that if you never had to do with bash scripting,
it can be a bit brutal until you get it to work. 

{% highlight shell %}
# Run this from a feature branch
# it switches to master, pulls all the latest changes and re-bases the feature onto master
pull-and-rebase () {
    devBranch=$(git branch | grep \* | cut -d ' ' -f2-)
    onBranch="master"
    if [ "$devBranch" == "$onBranch" ]
    then
        echo -e "\e[31mCannot rebase branch onto itself\e[0m"
        return 1
    fi
    git checkout "$onBranch"
    git pull --all
    git checkout "$devBranch" 
    git rebase "$onBranch"
}
{% endhighlight %}

{% highlight shell %}
# Pulls the latest origin with all branches
# And then cleans up all branches that were merged into master
# This is a sanitization function
cleanup-git-branches () {
    git checkout master
    git pull --all
	git remote prune origin
	git branch --merged master | grep -v -e "master" -e "development" | xargs -n 1 git branch -d
	git branch --list --all
}
{% endhighlight %}

[git functions link]:{{site.baseurl}}/assets/code/git-functions.sh