---
layout: post
title:  "Lost and found"
date:   2021-07-27 19:04:00 0300
categories: git interactive rebase
---

### {{page.title}}

Nothing is lost in git (usually). If you accidentally delete a branch,
the commits are not going anywhere. They can still be addressed and opened.

### Case study - my precious branch is gone, all gone :(

#### Step 1 - oops

Assume you have the following log

```
* d0103042e0 - (feature/X, origin/feature/X) Super feature X (1 minute ago) <John>
* 056a28c440 - Some bulk refactoring (1 days ago) <Mike>
* 329fbe465a - (HEAD -> master, origin/master, origin/HEAD) Add skeleton (31 hours ago) <Billy>
* 489a28ea04 - Initial commit with everything (2 days ago) <Denis>
...
```

```
* d0103042e0 - (feature/X, origin/feature/X) Super feature X (1 minute ago) <John>
* 056a28c440 - Some bulk refactoring (1 days ago) <Mike>
* 329fbe465a - (HEAD -> master, origin/master, origin/HEAD) Add skeleton (31 hours ago) <Billy>
* 489a28ea04 - Initial commit with everything (2 days ago) <Denis>
...
```

Then you zealously run `git branch -D feature/X` because your colleague told you that `-D` stands for **done**.

Now you run `gl` (a couple of times) and you start to panic.

```
* 329fbe465a - (HEAD -> master, origin/master, origin/HEAD) Add skeleton (31 hours ago) <Billy>
* 489a28ea04 - Initial commit with everything (2 days ago) <Denis>
...
```

#### Option 1 - you often use `gl`

Then it's easy. Scroll in your cli history and search for the hash.
Once you find it, you can run the command below.

``` shell
git checkout d0103042e0 # move HEAD to this hash
git checkout -b feature/X # Recreate the branch
```

#### Option 2 - you want hardcode and don't use git log

Well, this means that you need to go hunting for the commit.

If you remember the commit message, then try this command.

``` shell
git reflog --all | grep "Super feature"
```

OR

``` shell
git log --walk-reflogs --grep="Super feature"
```

If you have no recollection of the message, then you should just do `git reflog --all` and start browsing.
